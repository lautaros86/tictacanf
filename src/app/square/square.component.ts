import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-square',
  template: `
    <button class="square" 
    (click)="onClick()">
     {{value}}
    </button>
  `
})
export class SquareComponent {

  @Input()
  value: any;

  @Input()
  callback: Function;

  @Input()
  num: number;

  @Output()
  clickEvent: EventEmitter<number> = new EventEmitter<number>();

  onClick() {
    this.clickEvent.emit(this.num);
  }

}
