import { Component, OnInit, Input, AfterViewChecked, Output, EventEmitter } from '@angular/core';
import { HelperService } from '../helper.service';

@Component({
  selector: 'app-board',
  template: `
    <div>
      <div class="board-row">
        <app-square [num]="0"
                    [value]="squares[0]"
                    (clickEvent)="onClick($event)">
                    </app-square>
        <app-square [num]="1"
                    [value]="squares[1]"
                    (clickEvent)="onClick($event)">
                    </app-square>
        <app-square [num]="2"
                    [value]="squares[2]"
                    (clickEvent)="onClick($event)">
                    </app-square>
      </div>
      <div class="board-row">
        <app-square [num]="3"
                    [value]="squares[3]"
                    (clickEvent)="onClick($event)">
                    </app-square>
        <app-square [num]="4"
                    [value]="squares[4]"
                    (clickEvent)="onClick($event)">
                    </app-square>
        <app-square [num]="5"
                    [value]="squares[5]"
                    (clickEvent)="onClick($event)">
                    </app-square>
      </div>
      <div class="board-row">
        <app-square [num]="6"
                    [value]="squares[6]"
                    (clickEvent)="onClick($event)">
                    </app-square>
        <app-square [num]="7"
                    [value]="squares[7]"
                    (clickEvent)="onClick($event)">
                    </app-square>
        <app-square [num]="8"
                    [value]="squares[8]"
                    (clickEvent)="onClick($event)">
                    </app-square>
      </div>
    </div>
  `
})
export class BoardComponent {

  @Output()
  clickEvent: EventEmitter<number> = new EventEmitter<number>();

  @Input()
  squares: any;

  onClick(num) {
    this.clickEvent.emit(num);
  }
}
