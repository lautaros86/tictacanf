import { Component, OnInit } from '@angular/core';
import { HelperService } from './helper.service';

@Component({
  selector: 'app-root',
  template: `
  <div class="game">

  <div class="game-board"
  (click)="checkWinner();">

    <div>

      <app-board
      [squares]="squares"
      (clickEvent)="onClick($event)"></app-board>

  </div>
  <div class="game-info">

    <div class="status">{{status}}</div>
    <ol>
      <li *ngFor="let move of history; let i = index;">
        <button (click)="jumpTo(move, i)">{{buttonMove(i)}}</button>
      </li>
    </ol>
  </div> 

</div>

<router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  history = [{
    squares: Array(9).fill(null)
  }]

  xIsNext: boolean = true;

  title = 'tictacang';

  status: string;

  winner: string;

  squares: any;

  stepNumber: number = 0;

  constructor(private service: HelperService) { }

  ngOnInit() {
    this.squares = this.history[this.stepNumber];
    this.status = `Next Player: ${this.xIsNext ? 'X' : 'O'}`;
  }

  checkWinner() {
    this.winner = this.service.calculateWinner(this.squares);
    if (this.winner) {
      this.status = `Winner: ${this.winner}`;
    } else {
      this.status = `Next Player: ${this.xIsNext ? 'X' : 'O'}`;
    }
  }

  onClick(pos) {
    const current = this.history[this.stepNumber];
    this.stepNumber++
    const squares = current.squares.slice();
    if (this.winner || this.squares[pos]) {
      return;
    }
    squares[pos] = this.xIsNext ? 'X' : 'O';
    this.squares = squares;
    if (this.stepNumber < this.history.length) {
      this.history = this.history.slice(0, this.stepNumber);
    }
    this.history.push({ squares: squares })
    this.xIsNext = !this.xIsNext;
  }

  buttonMove(i) {
    return i ?
      'Go to move #' + i :
      'Go to game start';
  }

  jumpTo(move, step) {
    this.stepNumber = step;
    this.squares = move.squares;
    this.xIsNext = (step % 2) === 0
  }

}
